<%@ include file="/apps/diea/commons/global.jsp" %>
<title itemprop="name"><c:set var="title" value="${pageProperties.pageTitle}" />
    <c:if test="${empty title}">
        <c:set var="title" value="${pageProperties['jcr:title']}" />
    </c:if> <c:if test="${title != null}">
        <c:out value="${title}" />
    </c:if></title>
<link rel="canonical" href="${requestScope.canonicalPath}" itemprop="url"/>